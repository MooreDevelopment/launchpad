import React from 'react';
import { connect } from 'react-redux';
import { startLogin } from '../actions/auth';

export const LoginPage = () => (
  <div className="box-layout">
    <div className="box-layout__box">
      <h1 className="box-layout__title">Boilerplate</h1>
      <p>tagline for app</p>
      <button type="button" className="button" onClick={startLogin}>Login with Google</button>
    </div>
  </div>
);

const mapStateToProps = () => undefined;
const mapDispatchToProps = (dispatch) => ({
  startLogin: () => dispatch(startLogin(true))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
