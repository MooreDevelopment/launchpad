import React from 'react';

const LoadingPage = () => (
  <div className="loader">
    <img alt="loding gif" className="loader__image" src="/images/loader.gif" />
  </div>
);

export default LoadingPage;
